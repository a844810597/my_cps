# INSTRUCTION

为了说明各部分是如何组织，我们以`addfrac.c`(一个简单的程序，输入两个分数，得到两个分数相加的结果)为例，简要介绍。

## 结构及功能介绍

1. source_doc

    其中包含`autograding.java`(用于对程序给出具体分数)的源文件，以及文档说明。由于我们需要做的内容不涉及此，故不作过多说明，详细请参阅`autograding.docx`。

2. makefile

        # Executed by Tango
        all:
            tar xvf autograde.tar
            cp addfrac.c judge/stu.c
            # cd to judg and run driver
            (cd judge; ./driver.sh)
        clean:
            rm -rf ./judge

    该文件是整个测试的入口，通过执行`make`命令即可自动完成所有工作，并输出分数。

    其中，`autograde.tar`用于存放打分系统的一系列文件，解压后文件夹为`judge`。在此之后我所说明介绍的部分均在此文件夹内。
    `addfrac.c`是学生提及的待打分程序源代码。

3. case_file

        1/1+1/1
        1/2+3/4
        3/4+5/5
        10/2+2/10
        -1/-2+-2/-1
        -1/2+1/-2
        100007/3+3/100007
        233/322+322/233

    其中是对于`addfrac`的测试输入。

4. driver.sh

    其中，`std`指标准答案，`stu`指需测试的学生答案。`std_output`和`stu_output`分别为对于测试用例的标准答案输出和测试输出。


        #!/bin/bash
        # Variable definition
        std_filename='std'
        stu_filename='stu'
        std_output='std.txt'
        stu_output='stu.txt'
        case_file="case_file"

        # Compile source file
        echo -e "Compiling ${std_filename}...\c"
        gcc $std_filename.c -o $std_filename.o
        status=$?
        # Exit if something wrong
        if [ $status -ne 0 ]
        then
            echo -e "\nFATA: cannot compile ${std_filename}.c (returned $status)"
            exit 1
        fi
        echo "Done"

        echo -e "Compiling ${stu_filename}...\c"
        gcc $stu_filename.c -o $stu_filename.o
        status=$?
        if [ $status -ne 0 ]
        then
            echo -e "\nFATA: cannot compile ${stu_filename}.c (returned $status)"
            exit 1
        fi
        echo "Done"

        # Get standard soulution if necessary
        ./makeresult.sh $std_filename.o $case_file > $std_output

        # Erase student output file
        > $stu_output

        # Get student output file
        ./makeresult.sh $stu_filename.o $case_file > $std_output
        # Check output files and print if different
        echo "Checking output file..."
        diff -u $std_output $stu_output
        status=$?
        if [ $status -eq 0 ]
        then
            echo "diff nowhere."
        fi
        # run grading program
        java -classpath commons-io-2.6.jar:. AutoGrading $std_filename.c $std_output $stu_filename.c $stu_output


    `driver.sh`先将标准答案即`std_filename.c`和学生的答案`stu_filename.c`（在`makefile`中将学生写的文件`addfrac.c`拷贝到`judge/stu.c`）编译。若编译成功则`echo "Done"`否则输出错误信息并退出。

    随后，`driver.sh`执行

        ./makeresult.sh $std_filename.o $case_file > $std_output

    得到对于`case_file`中测试用例到标准答案，并输出到`std_output`中,同理得到学生代码的测试输出。

    之后，对于两个输出文件`stu_output`和`std_output`，进行`diff`比对，并输出比对信息，若答案一样，则`echo "diff nowhere"`。并调用打分程序`autograding`给出具体的分数。

5. makeresult.sh


        #! /bin/bash
        solution=$1
        case=$2
        if [ ! -e $solution ]
        then
            echo "$solution does not exist"
        fi
        if [ ! -e $case ]
        then
            echo "$case does not exist."
            exit
        fi
        while read line
        do
            echo $line | ./$solution
            echo
        done < $case


    在`driver.sh`中，`makeresult.sh`其后有两个参数，分别为.o结尾的两个编译后的文件和`case_file`，对应为该文件中的`solution`和`case`。

    在`makeresult`中，我们需要将`case_file`中的每一行作为测试程序的输入。


## 关于其他类型题目

在此，我只介绍了对于一类，需要完成整个程序，并在标准输入`stdin`中输入，将结果输出到标准输出`stdout`，且输出结果具有唯一性的题目。对于其他类型的题目，可能只需要学生写一个功能函数，比如题目`max`（求两个数中的较大值），需要将输入作为形参传入，通过`return`值得到结果。我们就需要增加头文件和相应的`main`函数来使测试使用的单个函数能够完成功能，并按照我们需要的方式输出，并通过`preprocess.sh`在编译前将其拼接起来。

对于其他类型的题目，其形式不同，但其整体结构相似，还需要各位去完成。

## HOW TO?

### 在开始之前你需要阅读并遵守的

[中文文案排版指北](https://github.com/mzlogin/chinese-copywriting-guidelines)

### 你可能需要完成/修改的

- makefile文件
- `driver.sh`
- 用于通过测试用例得到结果的`makeresult.sh`文件
- 用于测试的case_file
- 其他对于特定题目所需要的特定操作文件
