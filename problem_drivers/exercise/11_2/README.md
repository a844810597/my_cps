### 11-2：

修改第 5 章的编程题 8，使其包含下列函数：

		void find_closest_flight(int desired_time，int *departure_time，int *arrival_time);

函数需查出起飞时间与 desired_time（用从午夜开始的分钟数表示）最接近的航班。该航班的起飞时间和抵达时间（也都用午夜开始的分钟数表示）将分别存储在 departure_time 和 arrival_time 所指向的变量中。

