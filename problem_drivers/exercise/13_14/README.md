﻿### 第 13 章编程题 14

修改第 8 章的编程题 16，使其包含如下函数：

     bool are_anagrams(const char *word1, const char *word2);
如果 word1 和 word2 指向的字符串是变位词，函数返回 true。



