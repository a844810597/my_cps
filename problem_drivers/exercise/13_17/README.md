﻿### 第 13 章编程题 17

修改第 12 章的编程题 2，使其包含如下函数：

     bool is_palindrome(const char *message);
如果 message 指向的字符串是回文，函数返回 true。


