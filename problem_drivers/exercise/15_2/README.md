### 项目15_2：修改 15.3 节的 justify 程序，在 read_word 函数（而不是 main 函数）中为被截短的单词的结尾存储 * 字符。（只需要上传包含 void read_word(char *word, int len) 函数的可编译文件，要求输入一个超过 20 位的字母然后自动截断并打印）

		Please Enter a word:ewfewmplekwfmlpfmwelfkmewfkomweio
		The word is:ewfewmplekwfmlpfmwel*
