#include<stdio.h>
#include<stddef.h>
struct s {
	char a;
	int b[2];
	float c;
}test;
void main()
{

	printf("a的大小: %d,  a的偏移量：%d\n", sizeof(test.a),offsetof(struct s,a));
	printf("b的大小: %d,  b的偏移量：%d\n", sizeof(test.b), offsetof(struct s, b));
	printf("c的大小: %d,  c的偏移量：%d\n", sizeof(test.c), offsetof(struct s, c));
	printf("空洞的总大小:%d", offsetof(struct s, c)- sizeof(test.b)- sizeof(test.a));


}
