#include <stdio.h>
#include <string.h>

int main(int argc, char *argv)
{
	int num;
    int single = 0;
    int ten_bit = 0;

    printf("Enter a two-digit number: ");

    // 获取用户输入的数字，存入num变量
	scanf("%d", &num);

    // 如果用户输入的不是两位数，则主函数返回1
    if(num > 99 || (num < 10 && num > -10) || num < -99 ) {
        return 1;
    } 

    // 分别获得个位和十位的数字
    single = num % 10;
    ten_bit = num / 10;

    if(num < 0) {
        ten_bit = 0 - ten_bit;
        single = 0 - single;
        int reversal_num = ten_bit + single * 10;
        printf("The reversal is: -%d%d\n", single, ten_bit);

    }
    else{
        printf("The reversal is:%d%d\n", single, ten_bit);
    }
    return 0;
}
