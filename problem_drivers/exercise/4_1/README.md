# 4_1 逆序打印两位数

## 题目

编写一个程序，要求用户输入一个两位数，然后按数位的逆序打印出这个数。程序会话应类似下面这样：

      Enter a two-digit number: 28
      The reversal is: 82

用 %d 读入两位数，然后分解成两个数字。

## 样例

### 样例一

      Enter a two-digit number: 23
      The reversal is: 32

### 样例二

      Enter a two-digit number: -65
      The reversal is: -56

## 数据范围

可以假设输入的数为 `n` 范围为，则 10 ≤ n ≤ 99 或 -99 ≤ n ≤ -10 。

按照题目内容中的输出为规范格式进行输出。

## 提示

1. 请勿遗漏负数的情况

2. 按照题目内容中的输出为规范格式进行输出