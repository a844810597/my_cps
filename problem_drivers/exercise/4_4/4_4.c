#include <stdio.h>
#include <string.h>
#define BUFFERSIZE 40

int main(int argc, char *argv)
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;
	int number;

	printf("Enter a number between 0 and 32767: ");
	fgets(buffer, sizeof(buffer), stdin);
    length = strlen(buffer);
    buffer[length] = '\0';
    sscanf(buffer, "%d", &number);
    printf("In octal, your number is : ");
    if (number < 4095)
    	printf("0");
    printf("%o\n", number);
}
