# 5_3 计算佣金

## 题目

修改5.2节的broker.c程序，同时进行下面两种改变。

> (a) 不再直接输入交易额，而是要求用户输入股票的数量和每股的价格；
>
> (b) 增加语句用来计算经济人竞争对手的佣金(少于2000股时佣金为每股 33 美元 + 3 美分，2000股或者更多股时佣金为每股 33 美元 + 2 美分)。在显示原有经纪人佣金的同时，也显示出竞争对手的佣金。

    Enter the number of shares: 2000
    Enter the value of per share: 30
    Commission: $221.00
    Competitor commission: $73.00

## 样例

### 样例一

    Enter the number of shares: 2000
    Enter the value of per share: 30
    Commission: $221.00
    Competitor commission: $73.00

### 样例二

    Enter the number of shares: 2771
    Enter the value of per share: 83
    Commission: $407.99
    Competitor commission: $88.42

## 数据范围

可以假设输入股票的数量为 `n` ，则范围为 n ≥ 0；每股的价格为 `i`，则范围为 i ≥ 0

## 提示

1. 股票数量必须为整数，但每股的数量可以为float，输出结果保留两位小数

2. 按照题目内容中的输出为规范格式进行输出。