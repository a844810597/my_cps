#include <stdio.h>

int main(void)
{
    float number, largest;
    largest = 0.0f;
    
    // 循环接受用户输入
    for(;;) {
        printf("Enter a number(0 to terminate): ");
        scanf("%f", &number);
        // 输入小于等于0则结束接受
        if (number <= 0.0f)
            break;
        // 如果用户输入的数比largest大，则将该数赋值给Largest
        if (number > largest)
            largest = number;
    }
    printf("The largest number entered was: %.3f", largest);

    return 0;
}
