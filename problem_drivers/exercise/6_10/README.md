# 6_10 判断更早的日期（改 5.9）

## 题目

第 5 章的编程题 9 要求编写程序判断哪个日期更早。泛化该程序，使用户可以输入任意个日期，用 0/0/0 提示输入结束，不再输入日期:

    Enter a data(mm/dd/yy): 3/6/08
    Enter a data(mm/dd/yy): 5/17/07
    Enter a data(mm/dd/yy): 6/3/07
    Enter a data(mm/dd/yy): 0/0/0
    5/17/07 is the earliest date

## 样例

### 样例一

    Enter a data(mm/dd/yy): 3/6/08
    Enter a data(mm/dd/yy): 5/17/07
    Enter a data(mm/dd/yy): 6/3/07
    Enter a data(mm/dd/yy): 0/0/0
    5/17/07 is the earliest date

### 样例二

    Enter a date (mm/dd/yy): 12/31/12
    Enter a date (mm/dd/yy): 1/1/12
    Enter a date (mm/dd/yy): 1/31/12
    Enter a date (mm/dd/yy): 12/1/12
    Enter a date (mm/dd/yy): 0/0/0
    1/1/12 is the earliest date

## 数据范围

1. 设输入的年份为`y`，则 00 ≤ y ≤ 99；

2. 设输入的月份为`m`，则 1 ≤ m ≤ 12;

3. 每月天数应满足每月应有的天数;

4. 以上所有数据都应为整数;

## 提示

1. 按照题目内容中的输出为规范格式进行输出。