#include <stdio.h>

int main(void)
{
    int num, denom, m, n, temp;

    printf("Enter a fraction: ");
    scanf("%d /%d", &num, &denom);

    m = num;
    n = denom;

    // 求出最大公约数为m
    while (n != 0) {
       temp = m % n;
       m = n;
       n = temp;
    }

    // 分子分母同除以最大公约数，即为最简分数
    printf("In lowest terms: %d/%d", num / m, denom / m);

    return 0;
}
