# 6_4 改进broker.c(5.2)

## 题目

在 5.2 节的 broker.c 程序中添加循环，以便用户可以输入多笔交易并且程序可以计算每次的佣金。程序在用户输入的交易额为 0 时终止:

    Enter value of trade: 30000
    Commission: $166.00

    Enter value of trade: 20000
    Commission: $144.00

    Enter value of trade: 0

## 样例

### 样例一

    Enter value of trade: 30000
    Commission: $166.00

    Enter value of trade: 20000
    Commission: $144.00

    Enter value of trade: 0

### 样例二

    Enter the value of trade: 6456
    Commission: $97.95
    Enter the value of trade: 894651
    Commission: $1060.19
    Enter the value of trade: 15165
    Commission: $127.56
    Enter the value of trade: 465165
    Commission: $666.68
    Enter the value of trade: 0

## 数据范围

设输入的交易额为 `n` ，则 n ≥ 0 。

## 提示

1. 按照题目内容中的输出为规范格式进行输出。