# 7_12 表达式求值

## 题目

编写程序对表达式求值：

    Enter an expression: 1+2.5*3
    Value of expression: 10.5

表达式中的操作数是浮点数，运算符是 +、=、*、和/。表达式从左向右求值（所有运算符的优先级都一样）。

## 样例

### 样例一

    Enter an expression: 1+2.5*3
    Value of expression: 10.5

### 样例二

    Enter an expression: 2+2.5*2-5/2
    Value of expression: 2.0

## 数据范围

1. 输入的数据应为浮点数的表达式;

2. 输出结果保留一位小数

## 提示

1. 按照题目内容中的输出为规范格式进行输出。