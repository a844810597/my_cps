### 7-4：

  编写程序可以把字母 格式的电话号码翻译成数值格式：

		Enter phone number.CALLATT
		2255288
（如果没有电话在身边，参考这里给出的数字在键盘上的对应关系：2 = ABC , 3 = DEF , 4 = GHI , 5 = JKL , 6 = MNO , 7 = PRS , 8 = TUV , 9 = WXY。）原始电话号码中的非字母字符（例如数字或标点符号）保持不变：

		Enter phone number: 1-800-COL-LECT
		1-800-265-5328  