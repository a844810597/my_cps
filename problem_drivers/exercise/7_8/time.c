#include<stdio.h>
#include<math.h>
#include<stdlib.h>
int main()
{
	char i1,i2,i3;
	int choice, ip_time1, ip_time2, cl_time;
	int a[] = { 480,583,679,787,840,945,1140,1305 };
	int i;
	printf("Enter a 12-hour time:");
	scanf("%d:%d%c%c", &ip_time1, &ip_time2,&i1,&i2);
	if(i1=='p'||i1=='P'||i2=='P'||i2=='p')
	cl_time = (ip_time1+12) * 60 + ip_time2;
	if(i1=='a'||i1=='A'||i2=='A'||i2=='a')
	cl_time = ip_time1 * 60 + ip_time2;
	choice = 1305;

	for (i = 0; i <= 7; i++)
	{
		choice = (abs(cl_time - choice) > abs(cl_time - a[i]) ? a[i] : choice);
	}
	switch (choice) {

	case 480:
		printf("Closest departure time is 8:00 a.m., arriving at 10:16 a.m.");
		break;
	case 583:
		printf("Closest departure time is 9:43 a.m., arriving at 11:52 a.m.");
		break;
	case 679:
		printf("Closest departure time is 11:19 a.m., arriving at 1:31 p.m.");
		break;
	case 787:
		printf("Closest departure time is 12:47 p.m., arriving at 3:00 p.m.");
		break;
	case 840:
		printf("Closest departure time is 2:00 p.m., arriving at 4:08 p.m.");
		break;
	case 945:
		printf("Closest departure time is 3:45 p.m., arriving at 5:55 p.m.");
		break;
	case 1140:
		printf("Closest departure time is 7:00 p.m., arriving at 9:20 p.m.");
		break;
	case 1305:
		printf("Closest departure time is 9:45 p.m., arriving at 11:58 p.m.");
		break;
	}
	return 0;
}