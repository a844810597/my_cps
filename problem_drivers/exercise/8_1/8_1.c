#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFERSIZE 40

int main(int argc, char *argv)
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;
	char temp;
	int result[10] = {0};

    printf("Enter a number: ");
	fgets(buffer, sizeof(buffer), stdin);
    length = strlen(buffer);
    buffer[length] = '\0';
    length = length - 1;
    char number[length];
	int digit[length];
    sscanf(buffer, "%s", number);

    for(int i = 0; i < length; i++) {
    	char temp = number[i];
    	digit[i] = atoi(&temp);
    }
    
    for(int i = 0; i < length; i++) {
        result[digit[i]]++;
    }

    printf("Repeated digit(s): ");

    for(int i = 0; i < 10; i++) {
    	if(result[i] > 1) printf("%d ",i);
    }
    printf("\n");

}
