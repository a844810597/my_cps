#include <stdio.h>
#include <string.h>

#define N 8
#define BUFFERSIZE 40

struct flight {
	char *departure;
	char *arriving;
	int  time;
};

const struct flight search_flight[] =
	{{"8:00 a.m.", "10:16 a.m.", 480},
     {"9:43 a.m.", "11:52 a.m.", 583},
     {"11:19 a.m.", "1:31 p.m.", 679},
     {"12:47 p.m.", "3:00 p.m.", 767},
     {"2:00 p.m.", "4:08 p.m.", 840},
     {"3:45 p.m.", "5:55 p.m.", 945},
     {"7:00 p.m.", "9:20 p.m.", 1140},
     {"9:45 p.m.", "11:58 p.m.", 1305},
	};

int main(int argc, char *argv[])
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int number1 = 0;
	int number2 = 0;
	int result = 0;
	int index = 0;

	printf("Enter a 24-hour time: ");
	fgets(buffer, sizeof(buffer), stdin);
	sscanf(buffer, "%d:%d", &number1, &number2);
	result = number1 * 60 + number2;

	for (int i = 0; i < N; i++)
	{
		if (result < search_flight[i].time)
		{
			index = ((result - search_flight[i-1].time) \
< (search_flight[i].time - result)) ? (i - 1) : (i);
			break;
		}
	}

	printf("Closest departure time is %s, arriving at %s\n", \
search_flight[index].departure, search_flight[index].arriving);

	return 0;
}
