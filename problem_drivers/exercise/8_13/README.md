﻿### 第 8 章编程题 13

修改第 7 章的编程题 11，给输出加上标签：

     Enter a first and last name： Lloyd Fosdick
     You entered the name： Fosdick，L.
在显示姓氏（不是名字）之前，程序需要将其存储在一个字符数组中。可以假定姓氏的长度不超过 20
个字符。



