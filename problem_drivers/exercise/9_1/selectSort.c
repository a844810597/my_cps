#include <stdio.h>

/**
 * 如果排序成功返回0，排序失败返回1
 * 排序的顺序为升序
 */
int selection_sort(int array[], int n){
	int a
	// 异常参数传入
	/*
	if(n < 1 || array == NULL){
		return 1;
	}
	*/
	// 排序到最后一次返回
	if(n == 1){
		return 0;
	}
	for(int i = 0; i < n - 1; i++){
		if(array[i] > array[i+1]){
			int temp = array[i];
			array[i] = array[i+1];
			array[i+1] = temp;
		}
	}
	return selection_sort(array, n-1);
}
